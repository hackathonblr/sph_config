"use strict";
//controllers
const productController = require("./product.controller.js");
const configController = require("./config.controller.js");

//exports
var exports = {};
exports.v1_configCreate = configController.create;
exports.v1_configList = configController.index;
exports.v1_configShow = configController.show;
exports.v1_configDestroy = configController.destroy;
exports.v1_configUpdate = configController.update;

exports.v1_productCreate = productController.create;
exports.v1_productList = productController.index;
exports.v1_productShow = productController.show;
exports.v1_productDestroy = productController.destroy;
exports.v1_productUpdate = productController.update;

module.exports = exports;
    