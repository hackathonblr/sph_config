"use strict";

const mongoose = require("mongoose");
const definition = require("../helpers/product.definition.js").definition;
const SMCrud = require("swagger-mongoose-crud");
const cuti = require("cuti");
const schema = new mongoose.Schema(definition);
const logger = global.logger;

var options = {
    logger:logger,
    collectionName:"Product"
};
schema.pre("save", cuti.counter.getIdGenerator("PRD", "product"));
var crudder = new SMCrud(schema,"Product", options);
module.exports = {
    create:crudder.create,
    index:crudder.index,
    show:crudder.show,
    destroy:crudder.markAsDeleted,
    update:crudder.update
};
    