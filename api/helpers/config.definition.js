var definition = {
    "_id": {
        "type": "String",
        "default": null
    },
    "name": {
        "type": "String"
    },
    "product": {
        "type": "String"
    },
    "condition": {
        "type": "String"
    },
    "action": {
        "type": "String"
    },
    "message": {
        "type": "String"
    },
    "seq": {
        "type": "Number"
    }
};
module.exports.definition=definition;